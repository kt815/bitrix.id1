<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?


// dump($arResult['ITEMS']);

// Собираем ID
$arTempID = array();
foreach($arResult["ITEMS"] as $elem)
{

//dump($elem["ID"]);
$arTempID[] = $elem["ID"];

}

// dump($arTempID);

$arSort = false;
$arFilter = array(
	'IBLOCK_ID' => IBLOCK_NEWS_ID,
	'ACTIVE' => "Y",
	"PROPERTY_LINK_CAT" => $arTempID,
);

// Делаем SELECT
$arGroupBy = false;
$arNavStartParams = array("nTopCount" => 200);
$arSelect = array("ID", "NAME", "PROPERTY_LINK_CAT");
$BDRes = CIBlockElement::GetList(
	$arSoft,
	$arFilter,
	$arGroupBy,
	$arNavSatrtParams,
	$arSelect
);



// Собираем результат запроса
$arResult["NEWS"] = array();

while($arRes = $BDRes->GetNext())
{
	$arResult["NEWS"][$arRes["PROPERTY_LINK_CAT_VALUE"]] = $arRes;
}

// dump($arResult["NEWS"]);

foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$arItem['PRICES']['PRICE']['PRINT_VALUE'] = number_format($arItem['PRICES']['PRICE']['PRINT_VALUE'], 0, '.', ' ');
	$arItem['PRICES']['PRICE']['PRINT_VALUE'] .= ' '.$arItem['PROPERTIES']['PRICECURRENCY']['VALUE_ENUM'];
	$arResult['ITEMS'][$key] = $arItem;
}
?>