<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach($arResult["ITEMS"] as $ID=>$arItems)
{
$arImage=CFile::ResizeImageGet($arItems["DETAIL_PICTURE"], array('width'=>$arParams["LIST_PREV_PICT_W"],'height'=>$arParams["LIST_PREV_PICT_H"]), BX_RESIZE_IMAGE_PROPORTIONAL, true);
$arResult["ITEMS"][$ID]["DETAIL_PICTURE"]=$arImage;
}

$arTempID = array();
foreach($arResult["ITEMS"] as $elem)
{

//dump($elem["PROPERTIES"]["LINK_CAT"]["VALUE"]);
$arTempID[] = $elem["PROPERTIES"]["LINK_CAT"]["VALUE"];

}

// dump($arTempID);

$arSort = false;
$arFilter = array(
	'IBLOCK_ID' => 2,
	'ACTIVE' => "Y",
	"ID" => $arTempID,
);

$arGroupBy = false;
$arNavStartParams = array("nTopCount" => 50);
$arSelect = array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_PRICE", "PROPERTY_SIZE");
$BDRes = CIBlockElement::GetList(
	$arSoft,
	$arFilter,
	$arGroupBy,
	$arNavSatrtParams,
	$arSelect
);

$arResult["CAT_ELEM"] = array();

while($arRes = $BDRes->GetNext())
{
	$arResult["CAT_ELEM"][$arRes["ID"]] = $arRes;
}

?>

