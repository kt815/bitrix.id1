<?

function AgentChekPrice()
{

if(CModule::IncludeModule("iblock"))
{
	$arSelect = Array("ID", "NAME", "PROPERTY_PRICE");
	$arFilter = Array("IBLOCK_ID"=> CATALOG_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_PRICE" => false);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);


	while($ob = $res->GetNextElement())
	{
	 $arFields = $ob->GetFields();
	 $arItems[] = $arFields;
	}

	CEventLog::Add(array(
			"SEVERITY" => "SECURITY",
			"AUDIT_TYPE_ID" => "CHECK_PRICE",
			"MODULE_ID" => "iblock",
			"ITEM_ID" => "",
			"DESCRIPTION" => "Проверка цен, нет цен для ".count($arItems)." элементов",
	));

	if(count($arItems) > 0)
	{
		$arFilter = Array(
				"GROUPS_ID" => Array(GROUP_ADMIN_ID)
		);
		$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
		$arEmail = array();
		while($arResUser = $rsUsers->GetNext())
		{
			$arEmail[] = $arResUser["EMAIL"];
		}

		if(count($arEmail) > 0)
		{
			$arEventFields = array(
					"TEXT" => "Проверка цен, нет цен для ".count($arItems)." элементов",
					"EMAIL" => implode(", ", $arEmail),
			);
			CEvent::Send("CHECK_CATALOG", "s1", $arEventFields);
		}
	}

}
	return "AgentChekPrice();";
}


?>